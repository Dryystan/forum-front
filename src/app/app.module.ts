import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {NavbarComponent} from './components/shared/navbar/navbar.component';
import {ConnexionComponent} from './pages/connexion/connexion.component';
import {SigninComponent} from './components/connexion/signin/signin.component';
import {SignupComponent} from './components/connexion/signup/signup.component';

import {JwtInterceptor} from './helpers/jwt.interceptor';
import {ForumHomeComponent} from './pages/forum/forum-home/forum-home.component';
import {ForumSectionComponent} from './pages/forum/forum-section/forum-section.component';
import { ForumDiscussionComponent } from './pages/forum/forum-discussion/forum-discussion.component';
import { CreateDiscussionComponent } from './components/forum/create-discussion/create-discussion.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CreateMessageComponent } from './components/forum/create-message/create-message.component';
import { EditMessageComponent } from './components/forum/edit-message/edit-message.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    SigninComponent,
    ConnexionComponent,
    SignupComponent,
    ForumHomeComponent,
    ForumSectionComponent,
    ForumDiscussionComponent,
    CreateDiscussionComponent,
    CreateMessageComponent,
    EditMessageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
