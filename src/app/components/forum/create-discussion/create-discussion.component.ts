import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "../../../auth/services/authentication.service";
import {UserWithoutPassword} from "../../../dtos/user/user-without-password.model";
import {AngularEditorConfig} from "@kolkov/angular-editor";
import {ForumService} from "../../../services/forum.service";

@Component({
  selector: 'app-create-discussion',
  templateUrl: './create-discussion.component.html',
  styleUrls: ['./create-discussion.component.scss']
})
export class CreateDiscussionComponent implements OnInit {

  sectionId!: number;
  discussionForm: FormGroup;
  user!: UserWithoutPassword | null;
  htmlContent = '';
  success: boolean = false;
  @Output()
  created: EventEmitter<boolean> = new EventEmitter<boolean>();

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Écrire ici...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
    ]
  };

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private authService: AuthenticationService, private forumService: ForumService) {
    this.user = this.authService.currentUserValue;
    this.sectionId = this.route.snapshot.params['id-section'];
    this.discussionForm = this.fb.group({
      title: ['', Validators.required],
      section: fb.group({id: this.sectionId}),
      author: fb.group({id: this.user?.id}),
      firstMessage: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  createDiscussion() {
    this.forumService.createDiscussion(this.discussionForm.value).subscribe(() => {
      this.success = true;
      this.created.emit(true);
    });
  }

}
