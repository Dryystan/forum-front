import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from 'src/app/auth/services/authentication.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent {

  signin: boolean = true;

  constructor(private router: Router,
              private authenticationService: AuthenticationService) {
    if(this.authenticationService.currentUserValue) {
      this.router.navigate(['/forum-accueil']);
    }
  }

  connexion() {
    this.signin = true;
  }

  inscription() {
    this.signin = false;
  }

  signed_up(signed: boolean) {
    this.signin = signed;
  }
}
