import {Component, OnInit} from '@angular/core';
import {ForumService} from "../../../services/forum.service";
import {ActivatedRoute} from "@angular/router";
import {DiscussionDto} from "../../../dtos/forum/discussion-dto.dto";
import {AuthenticationService} from "../../../auth/services/authentication.service";
import {UserWithoutPassword} from "../../../dtos/user/user-without-password.model";
import {MessageDto} from "../../../dtos/forum/message-dto.dto";

@Component({
  selector: 'app-forum-discussion',
  templateUrl: './forum-discussion.component.html',
  styleUrls: ['./forum-discussion.component.scss']
})
export class ForumDiscussionComponent implements OnInit {

  id: number;
  sectionid: number;
  discussion!: DiscussionDto;
  messages!: MessageDto[];
  user!: UserWithoutPassword | null;
  newMessage: boolean = false;
  isEditedMessage: boolean = false;
  clickedIndex!: number;

  constructor(private forumService: ForumService, private route: ActivatedRoute, private authService: AuthenticationService) {
    this.user = this.authService.currentUserValue;
    this.id = this.route.snapshot.params['id-discussion'];
    this.sectionid = this.route.snapshot.params['id-section'];
  }

  ngOnInit(): void {
    this.getDiscussion();
    this.getMessagesByDiscussionId(this.id);
  }

  getDiscussion() {
    return this.forumService.getDiscussion(this.id).subscribe(data => this.discussion = data);
  }

  getMessagesByDiscussionId(id: number){
    return this.forumService.getMessagesByDiscussionId(id).subscribe( data => this.messages = data);
  }

  isCurrentUserIdEqualsAuthorId(authorId: number): boolean {
    if (this.user?.id === authorId) {
      return true;
    } else {
      return false;
    }
  }

  addLike() {
    this.forumService.addLikeToDiscussion(this.id, this.user!.id).subscribe(data => this.getDiscussion());
  }

  createMessage(){
    this.newMessage = true;
  }

  editMessage(){
    this.isEditedMessage= true;
  }

  cancelMessage() {
    this.newMessage = false;
  }

}
