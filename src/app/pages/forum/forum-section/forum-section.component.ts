import {Component, OnInit} from '@angular/core';
import {ForumService} from "../../../services/forum.service";
import {ActivatedRoute} from "@angular/router";
import {SectionDto} from "../../../dtos/forum/section-dto.dto";

@Component({
  selector: 'app-forum-section',
  templateUrl: './forum-section.component.html',
  styleUrls: ['./forum-section.component.scss']
})
export class ForumSectionComponent implements OnInit {

  id: number;
  section!: SectionDto;
  newDiscussion: boolean = false;

  constructor(private forumService: ForumService, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params['id-section'];
  }

  ngOnInit(): void {
    this.getSection();
  }

  getSection() {
    return this.forumService.getSection(this.id)
      .subscribe(data => this.section = data);
  }

  createDiscussion() {
    this.newDiscussion = true;
  }

  cancelDiscussion() {
    this.newDiscussion = false;
  }
}
