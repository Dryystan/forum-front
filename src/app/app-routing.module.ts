import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConnexionComponent} from './pages/connexion/connexion.component';
import {ForumHomeComponent} from "./pages/forum/forum-home/forum-home.component";
import {ForumSectionComponent} from "./pages/forum/forum-section/forum-section.component";
import {ForumDiscussionComponent} from "./pages/forum/forum-discussion/forum-discussion.component";

const routes: Routes = [
  {
    path: "connexion",
    component: ConnexionComponent
  },
  {
    path: "forum-accueil",
    component: ForumHomeComponent
  },
  {
    path: "forum-section/:id-section",
    component: ForumSectionComponent
  },
  {
    path: "forum-discussion/:id-section/:id-discussion",
    component: ForumDiscussionComponent
  },
  {
    path: "**",
    redirectTo: "connexion"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
