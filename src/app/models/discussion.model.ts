import {Section} from "./section.model";
import {User} from "./user.model";
import {Message} from "./message.model";

export class Discussion {
  /**
   * id, unique
   */
  id: number;
  /**
   * title
   */
  title: string;
  /**
   * section
   */
  section: Section;
  /**
   * author
   */
  author: User;
  /**
   * messages
   */
  messages: Message[];
  /**
   * firstMessage
   */
  firstMessage: Message;
  /**
   * Users ayant liké la discussion
   */
  likes: User[];
  /**
   * Date de création de la discussion
   */
  createdAt: Date;

  constructor(id: number, title: string, section: Section, author: User, messages: Message[], firstMessage: Message, likes: User[], createdAt: Date) {
    this.id = id;
    this.title = title;
    this.section = section;
    this.author = author;
    this.messages = messages;
    this.firstMessage = firstMessage;
    this.likes = likes;
    this.createdAt = createdAt;
  }
}
