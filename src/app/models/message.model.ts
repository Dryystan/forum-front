import {User} from "./user.model";
import {Discussion} from "./discussion.model";

export class Message {
  /**
   * id, unique
   */
  id: number;
  /**
   * content
   */
  content: string;
  /**
   * author
   */
  author: User;
  /**
   * discussion
   */
  discussion: Discussion;
  /**
   * createdAt
   */
  createdAt: Date;
  /**
   * updatedAt
   */
  updatedAt: Date;


  constructor(id: number, content: string, author: User, discussion: Discussion, createdAt: Date, updatedAt: Date) {
    this.id = id;
    this.content = content;
    this.author = author;
    this.discussion = discussion;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
