import {Discussion} from "./discussion.model";

export class Section {
  /**
   * id, unique
   */
  id: number;
  /**
   * title
   */
  title: string;
  /**
   * detail
   */
  detail: string;
  /**
   * discussions
   */
  discussions: Discussion[];

  constructor(id: number, title: string, detail: string, discussions: Discussion[]) {
    this.id = id;
    this.title = title;
    this.detail = detail;
    this.discussions = discussions;
  }
}
