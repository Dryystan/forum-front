import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from 'src/environments/environment';
import {Observable} from "rxjs";
import {SectionDto} from "../dtos/forum/section-dto.dto";
import {DiscussionDto} from "../dtos/forum/discussion-dto.dto";
import {MessageDto} from "../dtos/forum/message-dto.dto";
import {UserWithoutPassword} from "../dtos/user/user-without-password.model";

@Injectable({
  providedIn: 'root'
})
export class ForumService {

  constructor(private http: HttpClient) {
  }

  getSections(): Observable<SectionDto[]> {
    return this.http.get<SectionDto[]>(`${environment.apiUrl}/api/sections`);
  }

  getSection(id: number): Observable<SectionDto> {
    return this.http.get<SectionDto>(`${environment.apiUrl}/api/sections/` + id);
  }

  getDiscussion(id: number): Observable<DiscussionDto> {
    return this.http.get<DiscussionDto>(`${environment.apiUrl}/api/discussions/` + id);
  }

  createDiscussion(discussion: DiscussionDto){
    return this.http.post<DiscussionDto>(`${environment.apiUrl}/api/discussions/`, discussion);
  }

  addLikeToDiscussion(id: number, user_id: Number){
    return this.http.post<DiscussionDto>(`${environment.apiUrl}/api/discussions/` + id, user_id);
  }

  getMessagesByDiscussionId(id: number): Observable<MessageDto[]> {
    return this.http.get<MessageDto[]>(`${environment.apiUrl}/api/messages/discussion-` + id);
  }

  createMessage(message: MessageDto){
    return this.http.post<MessageDto>(`${environment.apiUrl}/api/messages/`, message);
  }

  editMessage(message: MessageDto, id: number){
    return this.http.put<MessageDto>(`${environment.apiUrl}/api/messages/` + id, message);
  }

}
