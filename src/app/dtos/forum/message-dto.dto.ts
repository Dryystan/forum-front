import {UserWithoutPassword} from "../user/user-without-password.model";
import {DiscussionDto} from "./discussion-dto.dto";

export class MessageDto {
  /**
   * id, unique
   */
  id: number;
  /**
   * content
   */
  content: string;
  /**
   * author
   */
  author: UserWithoutPassword;
  /**
   * discussion
   */
  discussion: DiscussionDto;
  /**
   * createdAt
   */
  createdAt: Date;
  /**
   * updatedAt
   */
  updatedAt: Date

  constructor(id: number, content: string, author: UserWithoutPassword, discussion: DiscussionDto, createdAt: Date, updatedAt: Date) {
    this.id = id;
    this.content = content;
    this.author = author;
    this.discussion = discussion;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
