import {DiscussionDto} from "./discussion-dto.dto";

export class SectionDto {
  /**
   * id, unique
   */
  id: number;
  /**
   * title
   */
  title: string;
  /**
   * detail
   */
  detail: string;
  /**
   * discussions
   */
  discussions: DiscussionDto[];

  constructor(id: number, title: string, detail: string, discussions: DiscussionDto[]) {
    this.id = id;
    this.title = title;
    this.detail = detail;
    this.discussions = discussions;
  }
}
