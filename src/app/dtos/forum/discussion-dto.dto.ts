import {SectionDto} from "./section-dto.dto";
import {UserWithoutPassword} from "../user/user-without-password.model";
import {MessageDto} from "./message-dto.dto";

export class DiscussionDto {
  /**
   * id, unique
   */
  id: number;
  /**
   * title
   */
  title: string;
  /**
   * section
   */
  section: SectionDto;
  /**
   * author
   */
  author: UserWithoutPassword;
  /**
   * messages
   */
  messages: MessageDto[];
  /**
   * firstMessage
   */
  firstMessage: MessageDto;
  /**
   * likes
   */
  likes: UserWithoutPassword[];
  /**
   * Date de création de la discussion
   */
  createdAt: Date;


  constructor(id: number, title: string, section: SectionDto, author: UserWithoutPassword, messages: MessageDto[], firstMessage: MessageDto, likes: UserWithoutPassword[], createdAt: Date) {
    this.id = id;
    this.title = title;
    this.section = section;
    this.author = author;
    this.messages = messages;
    this.firstMessage = firstMessage;
    this.likes = likes;
    this.createdAt = createdAt;
  }
}
